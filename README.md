# GitHub API app

Use this application if you want to know something about public repositories on GitHub. Find them by name!

## Quick guide

First clone repository.
Open the folder with application and run `npm update`.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.