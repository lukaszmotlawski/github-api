export interface Repositories {
    repositories: Repository[];
    totalCount: number;
}

export interface Repository {
    name: string;
    fullName: string;
    url: string;
}

export interface RepositoryExpanded {
    name: string;
    fullName: string;
    userLogin: string;
    language: string;
    defaultBranch: string;
}