import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DisplayRepositoryRoutingModule } from './display-repository-routing.module';
import { DisplayRepositoryComponent } from './display-repository.component';


@NgModule({
    declarations: [
        DisplayRepositoryComponent
    ],
    imports: [
        CommonModule,
        DisplayRepositoryRoutingModule
    ]
})
export class DisplayRepositoryModule { }
