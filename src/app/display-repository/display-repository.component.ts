import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { Repository, RepositoryExpanded } from '../interfaces/github-interfaces';
import { GithubApiService } from '../services/github-api-service';

@Component({
    selector: 'display-repository',
    templateUrl: './display-repository.component.html',
    styleUrls: ['./display-repository.component.scss']
})
export class DisplayRepositoryComponent implements OnInit, OnDestroy {
    public repositoryName: string;
    public routeSubscription: Subscription;
    public repositorySubscription: Subscription;
    public repository: RepositoryExpanded;

    public constructor(private route: ActivatedRoute, private githubService: GithubApiService) { }

    public ngOnInit(): void {
        this.routeSubscription = this.route.params.subscribe((params: Params) => {
            this.repositoryName = params['url'];

            this.repositorySubscription = this.githubService.getRepository(this.repositoryName).subscribe((response: any) => {
                this.repository = this.parseRepository(response);
            });
        });
    }

    public ngOnDestroy(): void {
        this.routeSubscription.unsubscribe();
        this.repositorySubscription.unsubscribe();
    }

    private parseRepository(data: any): RepositoryExpanded {
        let repository: RepositoryExpanded = {
            name: data.name,
            fullName: data.full_name,
            userLogin: data.owner.login,
            language: data.language,
            defaultBranch: data.default_branch
        }

        return repository;
    }
}
