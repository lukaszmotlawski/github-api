import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisplayRepositoryComponent } from './display-repository.component';

const routes: Routes = [{ path: '', component: DisplayRepositoryComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DisplayRepositoryRoutingModule { }
