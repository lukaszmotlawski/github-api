import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'repositoryFinder',
        loadChildren: () => import('./repository-finder/repository-finder.module').then(m => m.RepositoryFinderModule)
    },
    {
        path: 'displayRepository/:url',
        loadChildren: () => import('./display-repository/display-repository.module').then(m => m.DisplayRepositoryModule)
    },
    {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
