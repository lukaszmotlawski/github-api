import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RepositoryFinderRoutingModule } from './repository-finder-routing.module';
import { RepositoryFinderComponent } from './repository-finder.component';


@NgModule({
    declarations: [
        RepositoryFinderComponent
    ],
    imports: [
        CommonModule,
        RepositoryFinderRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class RepositoryFinderModule { }
