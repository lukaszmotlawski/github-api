import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GithubApiService } from '../services/github-api-service';
import { Repositories, Repository } from '../interfaces/github-interfaces';
import { Subscription } from 'rxjs';

const STRIP_TAGS = '[^<>;\/{}%$]+$';

@Component({
    selector: 'repository-finder',
    templateUrl: './repository-finder.component.html',
    styleUrls: ['./repository-finder.component.scss']
})
export class RepositoryFinderComponent implements OnInit {

    public repositoryFinderForm: FormGroup;
    public repositories: Repositories;
    public searchFlag: boolean = false;
    public repositoriesSubscription: Subscription;

    constructor(private formBuilder: FormBuilder, private router: Router, private githubService: GithubApiService) {
        this.buildForm();
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.repositoriesSubscription.unsubscribe();
    }

    public findRepositories() {
        this.repositoriesSubscription = this.githubService.getRepositoresByName(this.repositoryFinderForm.value.name).subscribe((response: any) => {
            this.repositories = this.parseRepositoriesResponse(response);
        });
    }

    public goToRepository(url: string) {
        this.router.navigate(['displayRepository', url]);
    }

    private buildForm() {
        this.repositoryFinderForm = this.formBuilder.group({
            name: new FormControl('', Validators.compose([Validators.required, Validators.pattern(STRIP_TAGS)]))
        });
    }

    private parseRepositoriesResponse(data: any): Repositories {
        let repositories: Repositories = {
            repositories: [],
            totalCount: data.items.length
        }

        for(let repository of data.items) {
            repositories.repositories.push(this.parseRepository(repository));
        }

        return repositories;
    }

    private parseRepository(data: any): Repository {
        let repository: Repository = {
            name: data.name,
            fullName: data.full_name,
            url: data.url
        }

        return repository;
    }
}
