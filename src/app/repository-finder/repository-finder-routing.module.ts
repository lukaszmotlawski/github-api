import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepositoryFinderComponent } from './repository-finder.component';

const routes: Routes = [{ path: '', component: RepositoryFinderComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RepositoryFinderRoutingModule { }
