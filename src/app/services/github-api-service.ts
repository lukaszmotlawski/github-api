import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GithubApiService {

    constructor(private http: HttpClient) { }

    public getRepositoresByName(name: string): any {
        return this.http.get('https://api.github.com/search/repositories?q=' + name + '%20in:name&per_page=10');
    }
    
    public getRepository(url: string): any {
        return this.http.get(url);
    }
}
